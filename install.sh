#!/bin/bash

set -e

prefix="${1:-/opt/vzlu}"
tmp_folder="${2:-/opt/vzlu/tmp}"
vcom_folder=$PWD
echo "======================================"
echo "Installing VCOM into $prefix..."
mkdir -p $tmp_folder
cd $tmp_folder
echo "======================================"
echo "Downloading dependencies..."

#Absolute minimum dependencies
apt-get install -y pip git

#VCOM dependencies
apt-get install -y pkg-config libsocketcan-dev cargo libssl-dev

pip install meson ninja --break-system-packages

echo "======================================"
echo "Installing LibCSP..."
git clone https://gitlab.com/vzlu/sw/libcsp.git
cd libcsp
git checkout v1.6.3
meson setup --prefix "$prefix" --libdir lib --pkg-config-path "$prefix/lib/pkgconfig" \
  -Dcsp_use_ifc_kiss=true -Dcsp_usart_linux=true -Dcsp_use_ifc_can=true -Dcsp_can_socketcan=true \
  -Dcsp_have_libsocketcan=true build
cd build
ninja install
cd ../..

echo "======================================"
echo "Installing LibConsole..."
git clone https://gitlab.com/vzlu/sw/lib-console.git
cd lib-console
git checkout v0.3.0
meson setup --prefix "$prefix" --libdir lib --pkg-config-path "$prefix/lib/pkgconfig" -Duse_csp_commands=true -Dhave_csp=true build
cd build
ninja install
cd ../..

echo "======================================"
echo "Installing lpldgen..."
git clone https://gitlab.com/vzlu/sw/lpldgen.git
cd lpldgen
git checkout v1.4.5
meson setup --prefix "$prefix" --libdir lib --pkg-config-path "$prefix/lib/pkgconfig" build
cd build
ninja install
cd ../..

echo "======================================"
echo "Installing LibCommands..."
git clone https://gitlab.com/vzlu/sw/libcommands.git
cd libcommands
git checkout v0.1.0
meson setup --prefix "$prefix" --libdir lib --pkg-config-path "$prefix/lib/pkgconfig" build
cd build
ninja install
cd ../..

echo "======================================"
echo "Installing SALLY..."
git clone https://gitlab.com/vzlu/sw/sally.git
cd sally
git checkout v0.3.0
meson setup --prefix "$prefix" --libdir lib --pkg-config-path "$prefix/lib/pkgconfig" -Denable_csp=true build
cd build
ninja install
cd ../..

echo "======================================"
echo "Building VCOM..."
cd $vcom_folder
./build_plugins.sh

# cleanup
rm -rf $tmp_folder