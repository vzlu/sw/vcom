/**
 * \file nii.h
 * \brief Non Interactive Interpret - replacement of GOSH.
 * \author Petr Svoboda, 2015-02-11
 */

#ifndef NON_INTERACTIVE_INTERPRET_H
#define NON_INTERACTIVE_INTERPRET_H

extern int non_interactive_interpret_start(unsigned short port, const char *logpath);

#endif // NON_INTERACTIVE_INTERPRET_H
